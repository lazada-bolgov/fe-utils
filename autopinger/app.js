/**
 * @usage
 * node ./app.js
 */

var webdriver = require('selenium-webdriver');
var chrome = require('selenium-webdriver/chrome');
var path = require('chromedriver').path;
var service = new chrome.ServiceBuilder(path).build();
chrome.setDefaultService(service);


var hostGoblinNew = 'http://alice.go-1048.dev.moscow.russia:80',
    hostGoblinOld = 'http://alice.go-1048master.dev.moscow.russia:80',
    hostPhp = 'http://alice.go-1048php.dev.moscow.russia:80';

//var builder = new webdriver.Builder()
//    .withCapabilities(webdriver.Capabilities.chrome());
var builder = new webdriver.Builder()
    .forBrowser('firefox');

var By = webdriver.By,
    until = webdriver.until;

/**
 * This is it!
 * Main.
 */
loopBrowse(10000, [hostGoblinNew, hostGoblinOld, hostPhp], waitFor);

function waitFor(browser)
{
    browser.wait(until.elementLocated(By.id('google-root')), 1000)
        .thenCatch(function () { browser.sleep(1000); });
}

function browse(host, cb)
{
    var browser = builder.build();
    browser.get(host);

    if (cb) {
        cb(browser);
    }

    return browser.quit();
}


function loopBrowse(times, hosts, cb)
{
    if (times < 1) {
        times = 1;
    }

    if ("[object Array]" !== Object.prototype.toString.call(hosts)) {
        hosts = [hosts];
    }

    var lastIndex = -1;

    oneTick();

    function oneTick()
    {
        if (--times < 0) {
            return;
        }
        var curIndex = nextIndexFromArray(hosts, lastIndex);
        var host = hosts[curIndex];
        lastIndex = curIndex;

        var deferred = browse(host, cb);

        webdriver.promise.when(deferred, function () {
            oneTick();
        });
    }
}

function nextIndexFromArray(arr, lastIndex)
{
    if (lastIndex < 0 || lastIndex >= (arr.length - 1)) {
        return 0;
    }
    return ++lastIndex;
}